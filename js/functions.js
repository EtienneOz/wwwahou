function loadAndShowIframe(item){
  item.children('.infos').children('a').mouseenter(function(){
    var iframe = $(this).parents('.item').find('iframe');
    var src = iframe.attr('data-src');
    iframe.attr('src', src);
    iframe.parent('.iframe').show();
    iframe.contents().find('audio, video').each(function(){
       $(this).volume = 0.0;
    });
  }).mouseleave(function(){
    var iframe = $(this).parents('.item').find('iframe');
    var src = iframe.attr('src');
    var parent = iframe.parent('.iframe');
    iframe.remove();
    parent.hide();
    parent.append('<iframe data-src="' + src + '"></iframe>')
  })
}

function blurItems(item){
  item.children('.infos').mouseenter(function(){
    $('.item *:not(iframe)').css({'filter': 'blur(1px)'});
    var parentItem = $(this).parents('.item');
    parentItem.find('*').css({'filter': 'blur(0px)'});
  }).mouseleave(function(){
    $('.item *:not(iframe)').css({'filter': 'blur(0px)'});
  })
}

function showAbstract(item){
  item.mouseenter(function(){
    $(this).find('p').css({'display': 'inline-block'});
  }).mouseleave(function(){
    $(this).find('p').hide();
  })
}

function classContent(item){
  var filter = $('.tags').find('li');
  filter.click(function(){
    var theClass = $(this).attr('class').split(' ')[1];
    if (!$(this).hasClass('beyond')){
      $(this).addClass('beyond');
      item.removeClass('beyond').addClass('behind');
      item.filter('.'+theClass).addClass('beyond').removeClass('behind');
    } else {
      $(this).removeClass('beyond');
      if (!filter.hasClass('beyond')) {
        item.removeClass('beyond').removeClass('behind');
      } else{
        $('.item').find('.'+theClass).removeClass('beyond').addClass('behind');
      }
    }
  })
}

function spanLetters(el){
   el.each(function(){
     var letters = $(this).html().split('');
     for (var i = 0; i < letters.length; i++) {
       if (letters[i] == ' ') {
         letters[i] = '<span>&nbsp;</span>';
       }

       letters[i] = '<span>' + letters[i] + '</span>';
     }
     var newLink = letters.join('');
     $(this).html(newLink);
   })
 }

function marginItems(item){
  var i = .1;
  var j = $(window).width() / 1.5;
  item.each(function(){
    if (i < $(window).width() / 1.5) {
      $(this).css({marginLeft: i + 'px'});
      i = i * 1.2;
      j = $(window).width() / 1.5;
    } else if (j < .1){
      i = .1;
      $(this).css({marginLeft: i + 'px'});
      i = i * 1.2;
    } else {
      $(this).css({marginLeft: j + 'px'});
      j = j / 1.2;
    }
  })
}

function letterSpace(item){
  var i = 0;
  item.each(function(){
    $(this).children('span').css({'letter-spacing': i + 'em'})
    i = i + .01;
  })

}

function about(el){
  el.click(function(){
    $(this).children('.infos').toggle();
  })
}
